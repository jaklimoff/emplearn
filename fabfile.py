from __future__ import with_statement
from fabric.api import local, settings, abort
from fabric.contrib.console import confirm


def ci(message):
    with settings(warn_only=True):
        local("git add .")
        local("git ci -m'%s'" % message)
        result = local("git push", capture=True)
