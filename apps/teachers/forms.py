from __future__ import absolute_import
from django import forms

from apps.base.models import Lesson


class LessonCreateForm(forms.ModelForm):
    priority = forms.IntegerField(required=False)

    class Meta:
        model = Lesson
        fields = ['title', 'description', 'content', 'priority']