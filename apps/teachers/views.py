from __future__ import absolute_import

from django.core.urlresolvers import reverse
from django.shortcuts import render
from django.views import generic

from apps.base.models import Course, Lesson
from apps.teachers.forms import LessonCreateForm


class TeacherCoursesList(generic.ListView):
    template_name = "learn/teacher/courses.html"
    model = Course
    context_object_name = 'courses'

    def get_queryset(self):
        self.queryset = Course.objects.filter(admins=self.request.user)
        return super(TeacherCoursesList, self).get_queryset()

    def get(self, request, *args, **kwargs):
        return super(TeacherCoursesList, self).get(request, *args, **kwargs)


class TeacherDashboard(generic.TemplateView):
    template_name = "learn/teacher/dashboard.html"


class TeacherCourseEdit(generic.DetailView):
    template_name = "learn/teacher/course_edit.html"
    model = Course
    pk_url_kwarg = 'course_id'
    context_object_name = 'course'

    def get_context_data(self, **kwargs):
        kwargs['lessons'] = Lesson.objects.filter(course=self.object, parent=None)
        return super(TeacherCourseEdit, self).get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        return super(TeacherCourseEdit, self).get(request, *args, **kwargs)


class TeacherCourseCreate(generic.DetailView):
    template_name = "learn/teacher/course_create.html"
    model = Course
    context_object_name = 'courses'

    def get(self, request, *args, **kwargs):
        return super(TeacherCourseCreate, self).get(request, *args, **kwargs)


class TeacherLessonCreate(generic.CreateView):
    template_name = "learn/teacher/lesson_create.html"
    model = Lesson
    form_class = LessonCreateForm

    def get_success_url(self):
        return reverse('teacher-lesson-edit', kwargs={'lesson_id': self.object.id})

    def get(self, request, *args, **kwargs):
        return super(TeacherLessonCreate, self).get(request, *args, **kwargs)

    def post(self, request, *args, **kwargs):
        return super(TeacherLessonCreate, self).post(request, *args, **kwargs)

    def form_valid(self, form):
        course = Course.objects.get(id=self.kwargs.get('course_id'), admins=self.request.user)
        form.instance.course = course
        return super(TeacherLessonCreate, self).form_valid(form)

    def get_form(self, form_class=None):
        return super(TeacherLessonCreate, self).get_form(form_class)


class TeacherLessonEdit(generic.UpdateView):
    template_name = "learn/teacher/lesson_edit.html"
    model = Lesson
    pk_url_kwarg = 'lesson_id'
    form_class = LessonCreateForm

    def get_success_url(self):
        return reverse('teacher-lesson-edit', kwargs={'lesson_id': self.object.id})

    def get_context_data(self, **kwargs):
        return super(TeacherLessonEdit, self).get_context_data(**kwargs)

    def get(self, request, *args, **kwargs):
        return super(TeacherLessonEdit, self).get(request, *args, **kwargs)