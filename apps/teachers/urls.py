from __future__ import absolute_import
from django.conf.urls import url, include

from apps.base.views import MainPage
from apps.teachers.views import TeacherCoursesList, TeacherCourseCreate, TeacherDashboard, TeacherCourseEdit, \
    TeacherLessonCreate, TeacherLessonEdit

urlpatterns = [
    url(r'^dashboard$', TeacherDashboard.as_view(), name="teacher-dashboard"),
    url(r'^courses', TeacherCoursesList.as_view(), name="teacher-courses"),
    url(r'^addcourse', TeacherCourseCreate.as_view(), name="teacher-course-create"),
    url(r'^course_(?P<course_id>[0-9]+)$', TeacherCourseEdit.as_view(), name="teacher-course-edit"),
    url(r'^course_(?P<course_id>[0-9]+)/addlesson$', TeacherLessonCreate.as_view(), name="teacher-lesson-create"),
    url(r'^lesson_(?P<lesson_id>[0-9]+)', TeacherLessonEdit.as_view(), name="teacher-lesson-edit"),
]

