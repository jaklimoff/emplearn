from __future__ import absolute_import
from django.conf.urls import url, include

from apps.base.views import MainPage

urlpatterns = [
    url(r'^dashboard$', MainPage.as_view(), name="student-dashboard"),
    url(r'^courses', MainPage.as_view(), name="student-courses"),
]

