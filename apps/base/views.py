from django.views.generic import TemplateView


class MainPage(TemplateView):
    template_name = "learn/index.html"


class BlogPage(TemplateView):
    template_name = "learn/blog.html"