from django.conf import settings


def current_profile(request):
    return {'profile': request.profile}

