from __future__ import unicode_literals, absolute_import

from django.core.validators import MaxValueValidator, MinValueValidator
from django.utils import timezone

from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.db import models


class UserProfile(models.Model):
    user = models.OneToOneField(to=User)
    balance = models.IntegerField(_('balance'))
    student = models.BooleanField(default=False)
    teacher = models.BooleanField(default=False)

    def __unicode__(self):
        return "%s profile" % self.user.username


class Project(models.Model):
    title = models.CharField(_('title'), max_length=255)
    admins = models.ManyToManyField(to=User, related_name='project_admins')
    author = models.ForeignKey(to=User, related_name='project_author', null=True)
    created = models.DateTimeField(_("created"), default=timezone.now, editable=False)

    def __unicode__(self):
        return self.title


class Course(models.Model):
    admins = models.ManyToManyField(to=User, related_name='course_admins')
    author = models.ForeignKey(to=User, related_name='course_author', null=True)
    project = models.ForeignKey(to=Project, related_name='project', null=True, blank=True)
    title = models.CharField(_('title'), max_length=255)
    description = models.TextField(_('description'), blank=True)
    created = models.DateTimeField(_("created"), default=timezone.now, editable=False)
    start_date = models.DateTimeField(_("start date"), default=timezone.now)

    def __unicode__(self):
        return self.title


class Lesson(models.Model):
    course = models.ForeignKey(to=Course, null=True, default=None)
    priority = models.IntegerField(_('lesson_priority'), default=999)
    title = models.CharField(_('title'), max_length=255)
    description = models.TextField(_('description'), blank=True)
    content = models.TextField(_('content'), blank=True)
    parent = models.ForeignKey(to="self", null=True)
    created = models.DateTimeField(_("created"), default=timezone.now, editable=False)

    def childs(self):
        return Lesson.objects.filter(course=self.course, parent=self)

    def __unicode__(self):
        return self.title

    class Meta:
        ordering = ("-priority",)
        get_latest_by = "created"
        verbose_name = _("Lesson")
        verbose_name_plural = _("Lessons")


class Testimonial(models.Model):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    project = models.ForeignKey(Course, on_delete=models.CASCADE)
    created = models.DateTimeField(_("created"), default=timezone.now, editable=False)
    content = models.TextField(_('content'), blank=True)
    rating = models.IntegerField(_('rating'),
                                 validators=[MaxValueValidator(5), MinValueValidator(1)])


from apps.base.signals import register_signals

register_signals()
