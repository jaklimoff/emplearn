from __future__ import absolute_import
from django.conf.urls import url, include

from apps.base.views import MainPage

urlpatterns = [
    url(r'^$', MainPage.as_view(), name="main-page"),
    url(r'^profile$', MainPage.as_view(), name="profile"),

]

