from __future__ import unicode_literals

from django.apps import AppConfig


class AppApiConfig(AppConfig):
    name = 'app_api'
