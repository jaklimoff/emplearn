from __future__ import absolute_import

from django.conf import settings
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

from apps.base.models import UserProfile


def create_profile(sender, instance, created, **kwargs):
    user = instance
    if created:
        UserProfile.objects.create(user=user, balance=settings.STARTED_BALANCE)


def register_signals():
    post_save.connect(create_profile, sender=User)
