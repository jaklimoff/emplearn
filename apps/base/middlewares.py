from .models import UserProfile


class ProfileMiddleware(object):
    def process_view(self, request, view_func, view_args, view_kwargs):
        if request.user.is_active:
            request.profile, created = UserProfile.objects.get_or_create(user=request.user)
        else:
            request.profile = None
