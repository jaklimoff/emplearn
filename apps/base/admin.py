from __future__ import absolute_import
from django.contrib import admin
from django.contrib.auth.models import User

from apps.base.models import UserProfile, Course, Lesson


class ProfileInline(admin.TabularInline):
    model = UserProfile


class UserAdmin(admin.ModelAdmin):
    inlines = (ProfileInline,)


admin.site.unregister(User)
admin.site.register(User, UserAdmin)

admin.site.register(UserProfile)
admin.site.register(Course)
admin.site.register(Lesson)
